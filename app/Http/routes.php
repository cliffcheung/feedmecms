<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

/*
Route::get('/events/create', function () {
    return view('index');
});
*/

Route::resource('events', 'EventsController');
Route::resource('news', 'NewsController');

Route::get('press', 'PressController@index');
Route::get('press/create', 'PressController@create');
Route::get('press/{id}/edit', 'PressController@edit');
Route::get('press/{lang}/{id}', 'PressController@show');

Route::post('press/{id}', 'PressController@store');
Route::put('press/{id}', 'PressController@update');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
