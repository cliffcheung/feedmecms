<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Press;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class PressController extends Controller
{
	public function index(){
		$press = Press::latest()->get();
		return view('press.index')->with('press', $press);
	}

	public function create(){
		return view('press.create');
	}

	public function store(Request $request){
		$press = new Press;
		$press->en_title = $request->en_title;
		$press->zh_title = $request->zh_title;
		$press->en_content = $request->en_content;
		$press->zh_content = $request->zh_content;
		$press->created_at = Carbon::now();
		$press->updated_at = Carbon::now();
		$press->save();

		return redirect('press');
	}

	public function show($lang, $id){
		$press = Press::findOrFail($id);

		if(strcmp($lang, 'en') == 0){
			$press->title = $press->en_title;
			$press->content = $press->en_content;
		}else if(strcmp($lang, 'zh') == 0){
			$press->title = $press->zh_title;
			$press->content = $press->zh_content;
		}

		return view('press.show')->with('press', $press);
	}

	public function edit($id){
		$press = Press::findOrFail($id);

		return view('press.edit')->with('press', $press);
	}

	public function update(Request $request, $id){
		$press = Press::findOrFail($id);
		$press->en_title = $request->en_title;
		$press->zh_title = $request->zh_title;
		$press->en_content = $request->en_content;
		$press->zh_content = $request->zh_content;
		$press->updated_at = Carbon::now();
		$press->update();

		return redirect('press');
	}

	public function destory($id){
	}
}
