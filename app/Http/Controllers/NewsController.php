<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class NewsController extends Controller
{
	public function index(){
		$news = News::latest()->get();

		return view('news.index')->with('news', $news);
	}

	public function create(){
		return view('news.create');
	}

	public function store(Request $request){
		$news = new News;

		$news->title = $request->title;
		$news->description = $request->description;
		$news->content = $request->content;
		$news->created_at = Carbon::now();
		$news->updated_at = Carbon::now();
		$news->save();

		return redirect('news');
	}

	public function show($id){
		$news = News::findOrFail($id);
		return view('news.show')->with('news', $news);
	}

	public function edit($id){
		$news = News::findOrFail($id);
		return view('news.edit')->with('news', $news);
	}

	public function update(Request $request, $id){
		$news = News::findOrFail($id);
		$news->title = $request->title;
		$news->description = $request->description;
		$news->content = $request->content;
		$news->updated_at = Carbon::now();
		$news->update();

		return redirect('news');
	}

	public function destory($id){
	}
}
