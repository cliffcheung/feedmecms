<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Image;
use File;

class EventsController extends Controller
{
	public function index(){
		$event = Event::latest()->get();

		return view('events.index')->with('events', $event);
	}

	public function create(){
		return view('events.create');
	}

	public function store(Request $request){
		//Image::make($request->header_image);
		//Image::make(Input::file('header_image'))->save('photo/test.jpg');
		//echo Input::file('file')->getFilename;
		//dd($request->all());
		//Image::make($request->file('header_image'))->save('foo.jpg');
		//dd(File::extension($request->file('header_image')));

		$location = public_path() . '/photo/';
		$file_name = Carbon::now()->timestamp . '.jpg';
		Image::make($request->file('header_image'))->save($location . $file_name);

		$event = new Event;
		$event->title = $request->title;
		$event->description = $request->description;
		$event->header_image = $file_name;
		$event->created_at = Carbon::now();
		$event->updated_at = Carbon::now();
		$event->save();

		return redirect('events');
	}

	public function show($id){
		$event = Event::findOrFail($id);
		return view('events.show')->with('event', $event);
	}

	public function edit($id){
		$event = Event::findOrFail($id);

		return view('events.edit')->with('event', $event);
	}

	public function update(Request $request, $id){
		$event = Event::findOrFail($id);
		$event->title = $request->title;
		$event->description = $request->description;
		$event->header_image = $request->header_image;
		$event->updated_at = Carbon::now();
		$event->update();

		return redirect('events');
	}

	public function destory($id){
	}
}
