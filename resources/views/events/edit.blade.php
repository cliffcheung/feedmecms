@extends('layouts.master')

@section('content')
	<div>id: {{$event->id}}</div>
	<div>Title: {{$event->title}}</div>

	<form action="{{url('events', $event->id)}}" method="POST">
		<input name="_method" type="hidden" value="PUT">

		Title:
		<input name="title">
		Description:
		<input name="description">
		Header_image:
		<input name="header_image">

		<button type="submit">submit</button>
	</form>
@stop
