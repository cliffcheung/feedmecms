@extends('layouts.master')

@section('content')
	<form enctype="multipart/form-data" action="{{url('events')}}" method="POST">
		Title:
		<input name="title">
		Description:
		<input name="description">
		Header_image:
		<input name="header_image" type="file">


		<button type="submit">submit</button>
	</form>

	<form id="content_image_form" action="" method="POST">
		Content_image:
		<input id="content_image_input" name="content_image" type="file" multiple>
		<button type="submit">upload</button>
	</form>

	<script>
		var content_image_form = document.getElementById('content_image_form');
		var content_image_input = document.getElementById('content_image_input');

		content_image_form.addEventListener('submit', function(event){
			event.preventDefault();
			var formData = new FormData();
			console.log(content_image_input.files);
		});
	</script>
@stop
