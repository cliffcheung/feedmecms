@extends('layouts.master')

@section('content')
	<div>This is news index page</div>

	@foreach($news as $one_news)
		<div>id: {{$one_news->id}}</div>
		<div><a href="{{ action('NewsController@show', [$one_news->id]) }}">Title: {{$one_news->title}}</a></div>
	@endforeach
@stop
