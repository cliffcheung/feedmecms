@extends('layouts.master')

@section('content')
	<div>id: {{$news->id}}</div>
	<div>Title: {{$news->title}}</div>
	<div>content: {{$news->content}}</div>

	<form action="{{url('news', $news->id)}}" method="POST">
		<input name="_method" type="hidden" value="PUT">

		Title:
		<input name="title">
		Description:
		<input name="description">
		Content:
		<input name="content">

		<button type="submit">submit</button>
	</form>
@stop
