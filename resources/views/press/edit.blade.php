@extends('layouts.master')

@section('content')
	<form action="{{url('press', $press->id)}}" method="POST">
		<input name="_method" type="hidden" value="PUT">

		English Title:
		<input name="en_title">
		zh Title:
		<input name="zh_title">
		english Content:
		<input name="en_content">
		zh Content:
		<input name="zh_content">

		<button type="submit">submit</button>
	</form>
@stop
