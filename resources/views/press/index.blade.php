@extends('layouts.master')

@section('content')
	<div>This is press index page</div>

	@foreach($press as $one_press)
		<div>id: {{$one_press->id}}</div>
		<div><a href="{{ action('PressController@show', ['en', $one_press->id]) }}">Title: {{$one_press->en_title}}</a></div>
	@endforeach

	@foreach($press as $one_press)
		<div>id: {{$one_press->id}}</div>
		<div><a href="{{ action('PressController@show', ['zh', $one_press->id])}}">Title: {{$one_press->zh_title}}</a></div>
	@endforeach
@stop
